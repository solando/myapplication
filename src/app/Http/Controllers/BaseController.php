<?php

namespace App\Http\Controllers;


use App\Services\ListCustomer;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function addCustomer(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|numeric',
        ]);

        try {
            $client = new \GuzzleHttp\Client();
            $res = $client->request('POST', 'https://capicollect.com/api/v1/customer/add',[
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'json' => [
                    "merchantId"=> env('MERCHANT_ID'),
                    "name"=> $request->name,
                    "email"=> $request->email,
                    "phone"=> $request->phone,
                    "bvn"=> $request->bvn,
                    "bank_name"=> $request->bank_name,
                    "account_name"=> $request->account_name,
                    "account_number"=> $request->account_number,
                    "address"=> $request->address
                    ]
            ]);

        } catch (\Exception $exception) {
        return response()->json(['status' => false, 'message' => $exception->getMessage()], 500);
        }
        return response()->json(['status' => true, 'message' => 'Customer Account Created Successfully', 'data' => $res], 201);
    }

    public function showCustomer(Request $request)
    {
        try {
            $check = (new ListCustomer())->run();
        }catch (\Exception $exception){
            return response()->json(['status' => false, 'message' => $exception->getMessage()], 500);
        }
            return response()->json(['status' => true, 'message' => 'Customer List', 'data' => $check], 200);
    }

    public function showSingleCustomer(Request $request){
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        try{
            $client = new \GuzzleHttp\Client();
            $res = $client->request('POST', 'https://capicollect.com/api/v1/customer/show',[
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'json' => [
                    "merchantId"=> env('MERCHANT_ID'),
                    "email"=> $request->email,
                ]
            ]);

        }catch (\Exception $exception){
            return response()->json(['status' => false, 'message' => $exception->getMessage()], 500);
        }
        return response()->json(['status' => true, 'message' => 'Customer Information', 'data' => json_decode($res->getBody(), true)['data']], 200);
    }

    public function addDebit(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
            'customer_email' => 'required|email',
            'description' => 'required|string'
        ]);

        try {
            $client = new \GuzzleHttp\Client();

            $res = $client->request('POST', 'https://capicollect.com/api/v1/debits/instant/add', [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'json' => [
                    "merchantId"=> env('MERCHANT_ID'),
                    "amount"=> $request->amount,
                    "customer_email"=> $request->customer_email,
                    "description"=> $request->description
                ]
            ]);
        }catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], 500);
        }
        return response()->json(['status' => true, 'message' => 'Instant Debit Completed', 'data' => json_decode($res->getBody(), true)['data']], 200);
    }

    public function listDebit(Request $request)
    {
        try{
            $client = new \GuzzleHttp\Client();
            $res = $client->request('POST', 'https://capicollect.com/api/v1/debits/instant',[
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'json' => [
                    "merchantId"=> env('MERCHANT_ID'),
                ]
            ]);

        }catch (\Exception $exception){
            return response()->json(['status' => false, 'message' => $exception->getMessage()], 500);
        }
        return response()->json(['status' => true, 'message' => 'List of Instant Debits', 'data' => json_decode($res->getBody(), true)['data']], 200);
    }

    public function showDebit(Request $request)
    {
        $this->validate($request, [
            'reference' => 'required|string',
        ]);

        try {
            $client = new \GuzzleHttp\Client();
            $res = $client->request('POST', 'https://capicollect.com/api/v1/debits/instant/show',[
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'json' => [
                    "merchantId"=> env('MERCHANT_ID'),
                    "reference"=> $request->reference,
                ]
            ]);

        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], 500);
        }
        return response()->json(['status' => true, 'message' => 'Instant Debit', 'data' => json_decode($res->getBody(), true)['data']], 200);
    }

    public function downloadDebit(Request $request)
    {
        $this->validate($request, [
            'reference' => 'required|string',
        ]);

        try {
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', 'https://capicollect.com/api/v1/download',[
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'json' => [
                    "reference"=> $request->reference,
                ]
            ]);

        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], 500);
        }
        return response()->json(['status' => true, 'message' => 'Instant Debit Download', 'data' => json_decode($res->getBody(), true)['data']], 200);

    }
}
