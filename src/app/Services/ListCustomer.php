<?php

namespace app\Services;

use App\Services\BaseServiceInterface;

class ListCustomer implements BaseServiceInterface
{

    public function run()
    {
        try {
            $client = new \GuzzleHttp\Client();
            $res = $client->request('POST', 'https://capicollect.com/api/v1/customers', [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'json' => [
                    "merchantId"=>env('MERCHANT_ID')
                ]
            ]);
        }catch (\Exception $exception){
            return response()->json(['status' => false, 'message' => $exception->getMessage()], 500);
        }
        return json_decode($res->getBody(), true)['data'];
    }
}
