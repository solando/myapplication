<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/customer'], function () use ($router) {
    $router->post('/create', 'BaseController@addCustomer');
    $router->post('/show', 'BaseController@showCustomer');
    $router->post('/show/single', 'BaseController@showSingleCustomer');
});

$router->group(['prefix' => 'api/debit'], function () use($router) {
    $router->post('create','BaseController@addDebit');
    $router->post('list','BaseController@listDebit');
    $router->post('show','BaseController@showDebit');
    $router->post('download','BaseController@downloadDebit');
});

